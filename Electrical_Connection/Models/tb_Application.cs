//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Homer_MVC.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tb_Application
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tb_Application()
        {
            this.tb_Documents = new HashSet<tb_Documents>();
            this.tb_MainSwitchDetails = new HashSet<tb_MainSwitchDetails>();
        }
    
        public int ApplicationId { get; set; }
        public int ApplicantId { get; set; }
        public int RepId { get; set; }
        public string Preferred { get; set; }
        public string MeterTypes { get; set; }
        public Nullable<System.DateTime> ToDate { get; set; }
        public Nullable<System.DateTime> FromDate { get; set; }
        public string ReasonForChange { get; set; }
        public string ExistingMeterNo { get; set; }
        public string ExistingConnectionNo { get; set; }
        public Nullable<System.DateTime> DateSupplyRequired { get; set; }
        public string TypeOfSupply { get; set; }
        public string RequestedDemand { get; set; }
        public string RequestedPower { get; set; }
        public string Tariff { get; set; }
        public string NoOfPhases { get; set; }
        public string TarifScale { get; set; }
        public string StreetNumber { get; set; }
        public string StreetName { get; set; }
        public string UnitNumber { get; set; }
        public string UnitName { get; set; }
        public string Suburb { get; set; }
        public string Town { get; set; }
        public string ERFNo { get; set; }
        public string PostalCode { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public bool Received { get; set; }
        public string ApplicationType { get; set; }
        public string ENumber { get; set; }
    
        public virtual tb_Applicant tb_Applicant { get; set; }
        public virtual tb_ElectricalRepresentative tb_ElectricalRepresentative { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tb_Documents> tb_Documents { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tb_MainSwitchDetails> tb_MainSwitchDetails { get; set; }
    }
}
