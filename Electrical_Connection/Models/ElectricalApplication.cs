﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.ComponentModel;

namespace Homer_MVC.Models
{
    public class ElectricalApplication
    {
        [Key]
        public string ApplicationType { get; set; }
        [Required(ErrorMessage = "Please Enter E number")]
        public string ENumber { get; set; }
        [Required(ErrorMessage = "Please Enter Full Name")]
        public string FullName { get; set; }
        [Required(ErrorMessage = "Please Enter ID Number")]
        public string IdNo { get; set; }
        [Required(ErrorMessage = "Please Enter LAndline")]
        public string LandLine { get; set; }
        [Required(ErrorMessage = "Please Enter Email Address")]
        [EmailAddress(ErrorMessage = "Please Enter A Valid Email Address")]
        public string Email { get; set; }
        public string Cellphone { get; set; }
        public string PreferredContact { get; set; }
        public string PostalAddress { get; set; }
        public string Suburbs { get; set; }
        public string PostalCodes { get; set; }
        public string StreetNumber { get; set; }
        public string TypeOfApplication { get; set; }
        public string StreetName { get; set; }
        public string UnitNumber { get; set; }

        public string UnitName { get; set; }
        public string Suburb { get; set; }
        public string Town { get; set; }
        public string ERFNo { get; set; }
        public string PostalCode { get; set; }

        public double Latitude { get; set; }
        public double Longitude { get; set; }


        // ELECTRICAL REPRESENTATIVE

        public string ElecRepresentativePostalAddress { get; set; }
        public string ElecRepresentativePostalCode { get; set; }

        public string ElecRepresentativeLandLine { get; set; }
        public string ElecRepresentativeEmail { get; set; }
        public string ElecRepresentativeCellphone { get; set; }
        public string ElecRepresentativeRegistrationNo { get; set; }

        //DETAILS OF MAIN SWITCH
        public string VoltageExistingSite { get; set; }
        public string CurrentExistingSite { get; set; }
        public string FaultRatingExistingSite { get; set; }
        public string ProtectionDeviceExistingSite { get; set; }

        public string VoltageProposedSite { get; set; }
        public string CurrentProposedSite { get; set; }
        public string FaultRatingProposedSite { get; set; }
        public string ProtectionDeviceProposedSite { get; set; }

        //MainSwitchDetails
        public string VoltageExistingApplication { get; set; }
        public string CurrentExistingApplication { get; set; }
        public string FaultRatingExistingApplication { get; set; }
        public string ProtectionDeviceExistingApplication { get; set; }

        public string VoltageProposedApplication { get; set; }
        public string CurrentProposedApplication { get; set; }
        public string FaultRatingProposedApplication { get; set; }
        public string ProtectionDeviceProposedApplication { get; set; }


        public string MeterType { get; set; }
        public string prefferedConnectionType { get; set; }
        public string reasonForChange { get; set; }
        public string TypeOfSupply { get; set; }
        public string DocumentFor { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }

        public string Tariff { get; set; }
        public string TariffScale { get; set; }
        public string NoOfPhases { get; set; }

        //MainSwitchDetails

        

        //file
        public string FilePath { get; set; }
        public string ContentType { get; set; }
        public string Description { get; set; }
        public Nullable<System.DateTime> DateCreated { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }

        public string DateSupply { get; set; }

        public string ExistingMeterNo { get; set; }
        public string ExistingConnectionNo { get; set; }
        public string RequestedDemand { get; set; }
        public string RequestedPower { get; set; }

        public bool AcceptTermsAndCondition { get; set; }

        [DataType(DataType.Upload)]
        [Display(Name = "Upload File")]
        [Required(ErrorMessage = "Please choose file to upload.")]
        public string file
        {
            get; set;

        }



        [DataType(DataType.Upload)]
        [Display(Name = "Copy Of Application ID File")]
        [Required(ErrorMessage = "Please choose file to upload.")]
        public string CopyOfAppID
        {
            get; set;
        }
        [DataType(DataType.Upload)]
        [Display(Name = "Proof Of Ownership File")]
        [Required(ErrorMessage = "Please choose file to upload.")]
        public string ProofOfOwnership
        {
            get; set;
        }
        [DataType(DataType.Upload)]
        [Display(Name = "Approved Building Plans File")]
        [Required(ErrorMessage = "Please choose file to upload.")]
        public string ApprovedBuildingPlans
        {
            get; set;
        }
        [DataType(DataType.Upload)]
        [Display(Name = "Copy of ID Director File")]
        [Required(ErrorMessage = "Please choose file to upload.")]
        public string CopyofIDDirector
        {
            get; set;
        }

        [DataType(DataType.Upload)]
        [Display(Name = "Company Reg Document File")]
        [Required(ErrorMessage = "Please choose file to upload.")]
        public string CompanyRegDoc
        {
            get; set;
        }

        [DataType(DataType.Upload)]
        [Display(Name = "Public Benefit Reg Doc")]
        [Required(ErrorMessage = "Please choose file to upload.")]
        public string PublicBenefitRegDoc
        {
            get; set;
        }
        [DataType(DataType.Upload)]
        [Display(Name = "Temp Lease Parks Gardens")]
        [Required(ErrorMessage = "Please choose file to upload.")]
        public string TempLeaseParksGardens
        {
            get; set;
        }
        [DataType(DataType.Upload)]
        [Display(Name = "Letter Tribal Auth")]
        [Required(ErrorMessage = "Please choose file to upload.")]
        public string LetterTribalAuth
        {
            get; set;
        }
        [DataType(DataType.Upload)]
        [Display(Name = "ID")]
        [Required(ErrorMessage = "Please choose file to upload.")]
        public string ID
        {
            get; set;
        }

        [DataType(DataType.Upload)]
        [Display(Name = "Business Support Approval")]
        [Required(ErrorMessage = "Please choose file to upload.")]
        public string BusSupportAppr
        {
            get; set;
        }
        [DataType(DataType.Upload)]
        [Display(Name = "Property Key")]
        [Required(ErrorMessage = "Please choose file to upload.")]
        public string PropertyKey
        {
            get; set;
        }

        [DataType(DataType.Upload)]
        [Display(Name = "Copy of ID")]
        [Required(ErrorMessage = "Please choose file to upload.")]
        public string CopyofID
        {
            get; set;
        }
        [DataType(DataType.Upload)]
        [Display(Name = "Land Consent Doc(Vacant Land)")]
        [Required(ErrorMessage = "Please choose file to upload.")]
        public string LandConsent
        {
            get; set;
        }
        [DataType(DataType.Upload)]
        [Display(Name = "Technical Infor Consumption")]
        [Required(ErrorMessage = "Please choose file to upload.")]
        public string TechInforConsumption
        {
            get; set;
        }
    }

}