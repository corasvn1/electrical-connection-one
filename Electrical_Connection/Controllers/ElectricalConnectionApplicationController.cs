﻿using Homer_MVC.BusinessLogic;
using Homer_MVC.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Homer_MVC.Controllers
{
    public class ElectricalConnectionApplicationController : Controller
    {
        ElectricityConnectionBusiness electricitybusiness = new ElectricityConnectionBusiness();
        private object viewbag;

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(ElectricalApplication model, IEnumerable<HttpPostedFileBase> files, string TypeOfApplication)
        {
            string result = "";
            try
            {
                if(!string.IsNullOrEmpty(model.ENumber))
                {
                    model.ENumber = model.ENumber.Replace(model.ENumber, "E" + model.ENumber);
                }
                else
                {
                    model.ENumber = "N/A";
                }
               
                //Save Applicant Information
                int ApplicantId = electricitybusiness.AddApplicant(model);
                //Save Representative Information
                int RepId = electricitybusiness.AddRepresentative(model);
                //Save Application
                int ApplicationId = electricitybusiness.ReceiveApplication(model,ApplicantId, RepId);
                electricitybusiness.SaveApplicationHistory(model, ApplicantId, RepId);
                electricitybusiness.SaveMainSwitchDetails(model, ApplicationId);

                foreach (var file in files)
                {
                    if (file != null)
                    {
                        if (file.ContentLength > 0)
                        {
                            //WebClient client = new WebClient();
                            //NetworkCredential theNetworkCredential = new NetworkCredential("Administrator", "P@$$w0rd");
                            //client.Credentials = theNetworkCredential;

                            //Uri addy = new Uri(@"//BPM-SERVER/Users/Administrator/Desktop/Documents/" + file.FileName);

                            //WebClient client = new WebClient();
                            //NetworkCredential theNetworkCredential = new NetworkCredential("Smallworld1", "HelloWorld123");

                            //Uri addy = new Uri(@"//SMALLWORLDSVR/Users/Smallworld/Documents/Uploads/" + file.FileName);

                            //client.Credentials = theNetworkCredential;

                            //MemoryStream target = new MemoryStream();
                            //file.InputStream.CopyTo(target);
                            //byte[] data = target.ToArray();
                            //client.UploadData(addy, data);



                            var fileName = Path.GetFileName(file.FileName);

                            var path = Path.Combine(Server.MapPath("~/Images/"), fileName);
                            var paths = "http://10.21.3.197:82/Images/" + fileName;
                            file.SaveAs(path);

                            var FileName = file.FileName;
                            var ContentType = file.ContentType;
                            var FilePath = paths.ToString();

                            //Save Documents
                            result = electricitybusiness.AddDocuments(model, FilePath, FileName, ContentType, ApplicationId, TypeOfApplication);
                            
                        }
                    }
                   
                }
                TempData["Message"] = result;
                ViewData["Message"] = result;
               
            }
            catch (Exception ex)
            {
                @ViewBag.Error = "Error in the form";
            }
            //ModelState.Clear();
            //return View();
            return RedirectToAction("Index");
        }
        
    }
}
