﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using Homer_MVC.BusinessLogic;
using System.Web.Security;
using System.Net;
using System.Threading.Tasks;
using Homer_MVC.ActionFilters;
using Homer_MVC.ViewModels;
using Homer_MVC.Models;

namespace Homer_MVC.Controllers
{
    public class UserController : Controller
    {
        private EncryptDecryptQueryString encryptDecryptQueryString = new EncryptDecryptQueryString();
   //     private ContraventionDBEntities db = new ContraventionDBEntities();
 
       // private UserBL userbusiness = new UserBL();
       // private Email EmailController = new Email();
        ServiceReference2.CallTakingClient calltaking = new ServiceReference2.CallTakingClient();
        
        public JsonResult CheckPasswordMatch(string Password, string ConfirmPassword)
        {
            string response = string.Empty;

            if (Password != ConfirmPassword)
            {
                response = "Password Does Not Match";
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public ActionResult Login()
        {
            bool cookieExists = Request.Cookies.AllKeys.Contains("authcookies");
            if (cookieExists)
            {
                string CookiesUsername = Request.Cookies["authcookies"]["username"];
                string CookiesPassword = Request.Cookies["authcookies"]["password"];
                ServiceReference2.CallTakingClient obj = new ServiceReference2.CallTakingClient();

                string Password = CookiesPassword;
                var getUser = obj.CheckUserExist(CookiesUsername, CookiesPassword).FirstOrDefault();
               // var objec = db.tb_People.Where(a => a.Email.ToLower().Equals(CookiesUsername.ToLower()) && a.Password.Equals(Password) && a.IsDeleted == false && a.IsActive == true).FirstOrDefault();
                if (getUser != null)
                {                 
           
                        Session["AccessLevelId"] = getUser.AccessLevelId;
                        Session["UserName"] = getUser.Email.ToString();                        
                        Session["Loginuser"] = getUser.Name.ToString() + " " + getUser.Surname.ToString();
                        return RedirectToAction("Index", "Main");
                }
                else
                {                    
                    return View();                
                }
            }
            else
            {
        
            }
            return View();
        }
    
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(string password, string username)
        {
            ServiceReference2.CallTakingClient obj = new ServiceReference2.CallTakingClient();
            // username = "dunga.gibson@gmail.com";
            // string username = Request.Cookies["authcookies"]["username"];
            try
            {
                var getUser = obj.CheckUserExist(username, password).FirstOrDefault();

                if (ModelState.IsValid)
                {

                    if (getUser != null)
                    {
                        Session["AccessLevelId"] = getUser.AccessLevelId;
                        //Session["PeopleId"] = getmethod..Id;
                        Session["UserName"] = getUser.Email.ToString();
                        Session["Loginuser"] = getUser.Name.ToString() + " " + getUser.Surname.ToString();

                        if (getUser.RememberLogin == "True")
                        {
                            Response.Cookies["authcookies"]["username"] = getUser.Email.Trim();
                            // Response.Cookies["authcookies"]["password"] = getUser.p.Trim();
                            Response.Cookies["authcookies"].Expires = DateTime.Now.AddDays(30);
                            getUser.RememberLogin = "True";
                          //  db.SaveChanges();

                        }
                        else
                        {
                            getUser.RememberLogin = "False";
                            Response.Cookies["authcookies"]["username"] = "unknown";
                            Response.Cookies["authcookies"]["password"] = "unknown";
                            //  db.SaveChanges();
                        }

                        string originalPath = Request.Url.ToString();
                        int index = originalPath.IndexOf("?");
                        if (index > 0)
                        {
                            string newpath = originalPath.Substring(index + 1);

                            string queryString = encryptDecryptQueryString.Decrypt(newpath, "r0b1nr0y");

                            int indexOfEqualSign = queryString.IndexOf("=");
                            string taskId = queryString.Substring(indexOfEqualSign + 1);
                            int TaskId = Convert.ToInt32(taskId);

                            return RedirectToAction("Details", "Task", new { id = TaskId });
                        }
                        else
                        {
                            return RedirectToAction("Index", "Main");
                        }

                    }
                    else
                    {
                        // var exist = db.tb_People.Where(a => a.Email.ToLower().Equals(obj.Email.ToLower()) && a.IsDeleted == false && a.IsActive == true).FirstOrDefault();
                        var exist = obj.CheckUserExist(username, password).FirstOrDefault();
                        if (exist != null && password != null)
                        {
                            Session["Result"] = "IncorectPassword";
                        }

                        else
                        {
                            Session["Result"] = "UserDoesNotExist";
                        }

                        return RedirectToAction("SubmitPage");
                    }

                }
            }
            catch(Exception ex)
            {
                ViewBag.Error ="Please ensure your username and password are correct (i.e there are no empty spaces in your fields)";
            }
            return View(obj);
           
        }
        public ActionResult SubmitPage()
        {
            string Result = Session["Result"].ToString();
            if (Result == "Successfully")
            {
                ViewBag.Message = "Thank you. Please visit your email.";
            }
            else if (Result == "RegistrationSuccessfully")
            {
                ViewBag.Message = "User Added Successfully. Please Check Your Email To Activate Your Account";
            }
            else if (Result == "UnSuccessfully")
            {
                ViewBag.Message = "We can not find the email address provided in our system. Kindly go and register your account";
            }
            else if (Result == "PasswordChanged")
            {
                ViewBag.Message = "Password changed successfully";
            }
            else if (Result == "PasswordUnChanged")
            {
                ViewBag.Message = "Passwords do not match";
            }
            else if (Result == "PasswordCreated")
            {
                ViewBag.Message = "Password Created successfully";
            }
            else if (Result == "PasswordNotCreated")
            {
                ViewBag.Message = "Passwords do not match";
            }
            else if (Result == "IncorectPassword")
            {
                ViewBag.Message = "Incorrect Password Please Try Again";
            }
            else if (Result == "UserDoesNotExist")
            {
                ViewBag.Message = "User Does Not Exist.Please Register On The System";
            }
            else if (Result == "UserAlreadyExist")
            {
                ViewBag.Message = "The Username Provided Already Exist. Please try Another One";
            }

            return View();
        }


     

        public ActionResult NotActivated()
        {
            ViewBag.Message = "User Not Activated";
            ViewBag.Instructions = "Please go to your Emails and click on the activation link to activate your account";

            return View();
        }

        [HttpGet]
        public ActionResult ForgotPassword()
        {
            string originalPath = Request.Url.ToString();
            string newpath = originalPath.Substring(0, originalPath.LastIndexOf("/"));
            return View();
        }



        public string Decrypt(string cipherText)
        {
            string EncryptionKey = "MAKV2SPBNI99212";
            byte[] cipherBytes = Convert.FromBase64String(cipherText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(cipherBytes, 0, cipherBytes.Length);
                        cs.Close();
                    }
                    cipherText = Encoding.Unicode.GetString(ms.ToArray());
                }
            }
            return cipherText;
        }

        private string Encrypt(string clearText)
        {
            string EncryptionKey = "MAKV2SPBNI99212";
            byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    clearText = Convert.ToBase64String(ms.ToArray());
                }
            }
            return clearText;
        }

        public ActionResult LogOff()
        {
            Session["RoleName"] = null;
            Session["Loginuser"] = null;
            Session["UserId"] = null;
            Session["UserName"] = null;
            Session["ManagerLevelId"] = null;
            Session["CompanyId"] = null;
            Session["Loginuser"] = null;
            Session["FirstimeLogin"] = null;
            Session.Clear();
            Session.Abandon();
            return RedirectToAction("Login", "User");
        }

        public ActionResult Logout()
        {
            bool cookieExists = Request.Cookies.AllKeys.Contains("authcookies");
            if (cookieExists)
            {
                Response.Cookies["authcookies"]["username"] = "unknown";
                Response.Cookies["authcookies"]["password"] = "unknown";
            }
            return RedirectToAction("Login", "User");
        }
        //[HttpGet]
        //public ActionResult ChangePasswordInternal(int UserId)
        //{
        //    var GetUser = db.tb_People.ToList().FirstOrDefault(x => x.Id == UserId && x.IsActive == true && x.IsDeleted == false);
        //    if (GetUser != null)
        //    {
        //        ViewBag.Email = GetUser.Email;
        //    }
        //    return View();

        //}
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult ChangePasswordInternal(tb_People obj)
        //{
        //    if (obj.Cell == obj.Password)
        //    {
        //        string Password = Encrypt(obj.Password);
        //        var User = db.tb_People.ToList().FirstOrDefault(x => x.Email == obj.Email && x.IsActive == true && x.IsDeleted == false);
        //        User.Password = Password;
        //        db.SaveChanges();
        //        Session["Result"] = "PasswordChanged";
        //    }
        //    else
        //    {
        //        Session["Result"] = "PasswordUnChanged";
        //    }
        //    return RedirectToAction("SubmitPage", "User");
        //}
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Login(tb_People obj)
        //{
        //    string Password = Encrypt(obj.Password);

        //    if (ModelState.IsValid)
        //    {
        //        using (TaskMonEntities db = new TaskMonEntities())
        //        {
        //            var objec = db.tb_Users.Where(a => a.Email.ToLower().Equals(obj.Email.ToLower()) && a.PasswordHash.Equals(Password) && a.IsDeleted == false && a.IsActive == true).FirstOrDefault();
        //            if (objec != null)
        //            {
        //                var activation = db.tb_UserActivation.ToList().FirstOrDefault(x => x.UserId == objec.Id);

        //                if (activation == null)
        //                {
        //                    var rolename = db.tb_Role.ToList().FirstOrDefault(x => x.Id == objec.RoleId && x.IsActive == true && x.IsDeleted == false);
        //                    Session["RoleName"] = rolename.Name.ToString();
        //                    Session["UserId"] = objec.Id;
        //                    Session["UserName"] = objec.Email.ToString();
        //                    Session["ManagerLevelId"] = objec.ManagerLevelId;
        //                    Session["CompanyId"] = objec.CompanyId;
        //                    Session["Loginuser"] = objec.FirstName.ToString() + " " + objec.LastName.ToString();
        //                    Session["FirstimeLogin"] = objec.FirstTimeLogin;

        //                    if (obj.RememberLogin == true)
        //                    {
        //                        Response.Cookies["authcookies"]["username"] = obj.Email.Trim();
        //                        Response.Cookies["authcookies"]["password"] = Password.Trim();
        //                        Response.Cookies["authcookies"].Expires = DateTime.Now.AddDays(2);
        //                        objec.RememberLogin = true;
        //                        db.SaveChanges();

        //                    }
        //                    else
        //                    {
        //                        objec.RememberLogin = false;
        //                        db.SaveChanges();
        //                    }

        //                    string originalPath = Request.Url.ToString();
        //                    int index = originalPath.IndexOf("?");
        //                    if (index > 0)
        //                    {
        //                        string newpath = originalPath.Substring(index + 1);

        //                        string queryString = encryptDecryptQueryString.Decrypt(newpath, "r0b1nr0y"); 

        //                        int indexOfEqualSign = queryString.IndexOf("=");
        //                        string taskId = queryString.Substring(indexOfEqualSign + 1);
        //                        int TaskId = Convert.ToInt32(taskId);

        //                        return RedirectToAction("Details", "Task", new { id = TaskId });
        //                    }
        //                    else
        //                    {
        //                        return RedirectToAction("Index", "Main");
        //                    }
        //                }
        //                else
        //                {                           
        //                    return RedirectToAction("NotActivated"); 
        //                }
        //            }
        //            else
        //            {
        //                var exist = db.tb_Users.Where(a => a.Email.ToLower().Equals(obj.Email.ToLower()) && a.IsDeleted == false && a.IsActive == true).FirstOrDefault();
        //                if (exist != null)
        //                {
        //                    Session["Result"] = "IncorectPassword";
        //                }
        //                else
        //                {
        //                    Session["Result"] = "UserDoesNotExist";
        //                }

        //                return RedirectToAction("SubmitPage");                       
        //            }
        //        }
        //    }
        //    return View(obj);
        //}
        // [HttpGet]
        //public ActionResult Register()
        //{
        //    ViewBag.AccessLevelId = new SelectList(db.tb_AccessLevel.ToList().Where(x => x.IsDeleted == false && x.IsActive == true), "Id", "AccessLevel",1);
        //    ViewBag.SiteId = new SelectList(db.tb_Site.ToList().Where(x => x.IsDeleted == false && x.IsActive == true), "Id", "Site");


        //    return View();
        //}


        //[HttpPost]
        //[ValidateAntiForgeryToken]   
        //public async Task<ActionResult> Register(RegisterCompany_User model, int[] SiteId)
        //{
        //    if (ModelState.IsValid)
        //     {
        //        //Create a Company

        //        Guid activationCode = Guid.NewGuid();
        //        var search = db.tb_People.ToList().FirstOrDefault(x => x.Email.ToLower() == model.Email.ToLower() && x.IsDeleted == false && x.IsActive == true);
        //        if (search == null)
        //        {
        //            //Adding a User

        //                Session["PeopleId"] = userbusiness.AddUser(model, Convert.ToInt16(Session["CompanyId"]));

        //            int PeopleId = Convert.ToInt16(Session["PeopleId"]);
        //            // Adding a profile picture
        //            for (int i = 0; i < Request.Files.Count; i++)
        //            {
        //                var files = Request.Files[i];

        //                if (files != null && files.ContentLength > 0)
        //                {
        //                    var fileName = Path.GetFileName(files.FileName);
        //                    var path = Path.Combine(Server.MapPath("~/Images/ProfilePictures"), fileName);
        //                    tb_UserProfilePicture picture = new tb_UserProfilePicture()
        //                    {
        //                        FileName = fileName,
        //                        FilePath = Path.GetExtension(fileName),
        //                        UserId = PeopleId,
        //                        ContentType = files.ContentType,
        //                        IsDeleted = false,
        //                        IsActive = true,
        //                        DateCreated = DateTime.Now,
        //                    };
        //                    db.tb_UserProfilePicture.Add(picture);
        //                    db.SaveChanges();
        //                    files.SaveAs(path);
        //                }
        //            }
        //            //assigning Sites to a Person

        //            try
        //            {
        //                EncryptDecryptQueryString encryptDecryptQueryString = new EncryptDecryptQueryString();
        //                string passString = encryptDecryptQueryString.Encrypt(PeopleId.ToString(), "r0b1nr0y");
        //                string UserId = passString.Replace('/', '_');
        //                string final = UserId.Replace('+', '-');

        //                Email EML = new Email();
        //                await EML.SendEmail("User Registered Successfully", "Hello " + model.Name + " " + model.Surname + "<br/> <br/>" + "You have been successfully registered on the Task Mon System." + "<br/> <br/>" + "Your login details are as follows :" + "<br/>" +
        //                          "User Name: " + model.Email.ToLower() + "<br/>" +
        //                         "<br/>" + "<br /><a href = '" + string.Format("{0}://{1}/User/Activation/{2},{3}", Request.Url.Scheme, Request.Url.Authority, activationCode, final) + "'>Click here to activate your account.</a>" + "<br/>" + "<br/>" +
        //                         "Regards" + "<br/>" +
        //                         "Task Mon System", model.Email.ToLower(), "Task Mon System", "no-reply@coralite.co.za", PeopleId, activationCode.ToString());

        //                Session["Result"] = "RegistrationSuccessfully";
        //                return RedirectToAction("SubmitPage");
        //            }
        //            catch (Exception em)
        //            {
        //                return RedirectToAction("Register");
        //            }
        //        }
        //        else
        //        {
        //            Session["Result"] = "UserAlreadyExist";
        //            return RedirectToAction("SubmitPage");
        //        }

        //    }
        //    return View(model);
        //}

        //public string GetProfilePicture(int UserId)
        //{
        //    string img = "";
        //    var Picture = db.tb_UserProfilePicture.ToList().FirstOrDefault(x => x.IsDeleted == false && x.IsActive == true && x.UserId == UserId);
        //    if (Picture != null)
        //    {
        //        img = Picture.FileName;
        //    }
        //    else
        //    {
        //        string Alphabet = "";
        //        var User = db.tb_People.ToList().FirstOrDefault(x => x.Id == UserId && x.IsActive == true && x.IsDeleted == false);
        //        if (User != null)
        //        {
        //            Alphabet = User.Name.Substring(0, 1);

        //            img = Alphabet.ToUpper() + ".png";
        //        }

        //    }

        //    return img;
        //}
        //public JsonResult CheckUsernameAvailability(string Email)
        //{
        //    string response = string.Empty;
        //    var user = db.tb_People.FirstOrDefault(x => x.Email == Email && x.IsActive == true && x.IsDeleted == false);
        //    if(user!=null)
        //    {
        //        response = "Username already exist";
        //    }
        //    return Json(response, JsonRequestBehavior.AllowGet);
        //}



        //[HttpGet]
        //public ActionResult CreatePassword()
        //{
        //    string originalPath = Request.Url.ToString();
        //    int index = originalPath.IndexOf("=");
        //    string newpath = originalPath.Substring(index + 1);
        //    string newd = newpath.Replace("%3D", "=");
        //    string newF = newd.Replace("%2F", "/");
        //    string all = newF.Replace("%2B", "+");
        //    int UserId = Convert.ToInt32(encryptDecryptQueryString.Decrypt(all, "r0b1nr0y"));

        //    var GetUser = db.tb_People.ToList().FirstOrDefault(x => x.Id == UserId && x.IsActive == true && x.IsDeleted == false);
        //    if (GetUser != null)
        //    {
        //        ViewBag.Email = GetUser.Email;
        //    }
        //    return View();
        //}
        //[HttpPost]
        //public ActionResult CreatePassword(tb_People obj)
        //{
        //    if (obj.Cell == obj.Password)
        //    {
        //        string Password = Encrypt(obj.Password);
        //        var User = db.tb_People.ToList().FirstOrDefault(x => x.Email == obj.Email && x.IsActive == true && x.IsDeleted == false);
        //        User.Password = Password;
        //        db.SaveChanges();
        //        Session["Result"] = "PasswordCreated";
        //    }
        //    else
        //    {
        //        Session["Result"] = "PasswordNotCreated";
        //    }
        //    return RedirectToAction("SubmitPage", "User");
        //}

        //public ActionResult Activation()
        //{
        //    string originalPath = Request.Url.ToString();
        //    int index1 = originalPath.LastIndexOf("/");
        //    int startpoint = index1 + 1;
        //    int index2 = originalPath.LastIndexOf(",");
        //    int endpoint = index2 - startpoint;
        //    string newpath = originalPath.Substring(startpoint, endpoint);
        //    string UserId = originalPath.Substring(index2 + 1);
        //    string one = UserId.Replace('_', '/');
        //    string two = one.Replace('-', '+');
        //    Session["UserId"] = two;

        //    if (newpath != null)
        //    {
        //        Guid activationCode = new Guid(newpath);
        //        ContraventionDBEntities usersEntities = new ContraventionDBEntities();
        //        tb_UserActivation userActivation = usersEntities.tb_UserActivation.Where(p => p.ActivationCode == activationCode).FirstOrDefault();
        //        if (userActivation != null)
        //        {
        //            usersEntities.tb_UserActivation.Remove(userActivation);
        //            usersEntities.SaveChanges();
        //            ViewBag.Message = "Activation successful.";
        //        }
        //        else
        //        {
        //            ViewBag.Message = "User Already Activated";
        //        }
        //    }
        //    else
        //    {
        //        ViewBag.Message = "User Already Activated";
        //    }

        //    return View();
        //}


    }
}
