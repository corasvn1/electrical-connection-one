﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Homer_MVC.Models;
using System.Globalization;

namespace Homer_MVC.BusinessLogic
{
    public class ElectricityConnectionBusiness
    {


        public int AddApplicant(ElectricalApplication model)
        {
            try
            {
                using (var db = new ElectricalConnectionDBEntities())
                {
                    tb_Applicant app = new tb_Applicant();
                    app.FullName = model.FullName;
                    app.Cellphone = model.Cellphone;
                    app.IDNo = model.IdNo;
                    app.LandLine = model.LandLine;
                    app.PostalAddress = model.PostalAddress;
                    app.Suburb = model.Suburbs;
                    app.PostalCode = model.PostalCodes;
                    app.Email = model.Email;
                    app.PreferedContact = model.prefferedConnectionType;

                    app.IsActive = true;
                    app.IsDeleted = false;

                    db.tb_Applicant.Add(app);
                    db.SaveChanges();
                    return app.ApplicantId;
                }
            }
            catch(Exception ex)
            {
                return 0;
            }

        }

        public int AddRepresentative(ElectricalApplication model)
        {
            try
            {
                using (var db = new ElectricalConnectionDBEntities())
                {
                    tb_ElectricalRepresentative rep = new tb_ElectricalRepresentative();
                    rep.Cellphone = model.Cellphone;
                    rep.Email = model.Email;
                    rep.PaostalCode = model.PostalCode;
                    rep.Poastal_Address = model.PostalAddress;
                    rep.RegistrationNumber = model.ElecRepresentativeRegistrationNo;
                    rep.Landline = model.LandLine;

                    rep.IsActive = true;
                    rep.IsDeleted = false;

                    db.tb_ElectricalRepresentative.Add(rep);
                    db.SaveChanges();
                    return rep.RepId;
                }
            }
            catch(Exception ex)
            {
                return 0;
            }

        }

        public int ReceiveApplication(ElectricalApplication model, int? applicantId, int? representativeId)
        {
            using (var db = new ElectricalConnectionDBEntities())
            {
                int ApplicantId = Convert.ToInt32(applicantId);
                int RepresentativeId = Convert.ToInt32(representativeId);
                try
                {
                    tb_Application application = new tb_Application();
                    application.ApplicantId = ApplicantId;
                    application.RepId = RepresentativeId;
                    application.MeterTypes = model.MeterType;
                    application.Tariff = model.Tariff;
                    application.TarifScale = model.Tariff;
                    application.ENumber = model.ENumber;
                    application.ApplicationType = model.ApplicationType.ToLower();

                    application.RequestedPower = model.RequestedPower;
                    application.NoOfPhases = model.NoOfPhases;
                    application.TypeOfSupply = model.TypeOfSupply;

                    if (model.TypeOfSupply == "Unmetered Temporary Period (< 14 Days)")
                    {
                        application.ToDate = Convert.ToDateTime(model.ToDate);
                        application.FromDate = Convert.ToDateTime(model.FromDate);
                    }
                    if (model.TypeOfSupply != "Unmetered Temporary Period (< 14 Days)")
                    {
                        application.ToDate = DateTime.Now;
                        application.FromDate = DateTime.Now;
                    }

                    if (model.ApplicationType == "CHANGES")
                    {
                        application.ReasonForChange = model.reasonForChange;
                        application.ExistingMeterNo = model.ExistingMeterNo;
                        DateTime MyDateTime = DateTime.Now;
                        if (!string.IsNullOrEmpty(model.DateSupply))
                        {
                            //application.DateSupplyRequired = Convert.ToDateTime(model.DateSupply);
                            //Convert date to a current computer format.
                            CultureInfo MyCultureInfo = new CultureInfo("de-DE");
                            MyDateTime = (DateTime.Parse(model.DateSupply, MyCultureInfo));
                            application.DateSupplyRequired = MyDateTime;
                        }
                        else
                        {
                            application.DateSupplyRequired = MyDateTime;
                        }
                        application.ExistingConnectionNo = model.ExistingConnectionNo;
                    }
                    application.RequestedDemand = model.RequestedDemand;
                    application.Preferred = model.prefferedConnectionType;
                    application.Longitude = model.Longitude.ToString() == "" ? "N/A" : model.Longitude.ToString();
                    application.Latitude = model.Latitude.ToString() == "" ? "N/A" : model.Latitude.ToString();
                    application.ERFNo = model.ERFNo == null ? "N/A" : model.ERFNo;
                    application.PostalCode = model.PostalCode == null ? "N/A" : model.PostalCode;
                    application.StreetNumber = model.StreetNumber.ToString() == "" ? "N/A" : model.StreetNumber;
                    application.StreetName = model.StreetName.ToString() == "" ? "N/A" : model.StreetName;
                    application.UnitNumber = model.UnitNumber.ToString() == "" ? "N/A" : model.UnitNumber;
                    application.UnitName = model.UnitName == null ? "N/A" : model.UnitName;
                    application.Suburb = model.Suburb == null ? "N/A" : model.Suburb;
                    application.Town = model.Town == null ? "N/A" : model.Town;
                    application.ReasonForChange = model.reasonForChange == null ? "N/A" : model.reasonForChange;
                    application.ExistingMeterNo = model.ExistingMeterNo == null ? "N/A" : model.ExistingMeterNo;
                    application.ExistingConnectionNo = model.ExistingConnectionNo == null ? "N/A" : model.ExistingConnectionNo;


                    application.DateSupplyRequired = DateTime.Now;


                    application.IsActive = true;
                    application.IsDeleted = false;
                    application.Received = false;

                    db.tb_Application.Add(application);
                    db.SaveChanges();
                    return application.ApplicationId;
                }
                catch(Exception ex)
                {
                    return 0;
                }

            }


        }



        public void SaveApplicationHistory(ElectricalApplication model, int? applicantId, int? representativeId)
        {
            using (var db = new ElectricalConnectionDBEntities())
            {
                int ApplicantId = Convert.ToInt32(applicantId);
                int RepresentativeId = Convert.ToInt32(representativeId);
                try
                {
                    tb_Application_History application = new tb_Application_History();
                    application.ApplicantId = ApplicantId;
                    application.RepId = RepresentativeId;
                    application.MeterTypes = model.MeterType;
                    application.Tariff = model.Tariff;
                    application.TarifScale = model.Tariff;
                    application.ENumber = model.ENumber;
                    application.ApplicationType = model.ApplicationType.ToLower();

                    application.RequestedPower = model.RequestedPower;
                    application.NoOfPhases = model.NoOfPhases;
                    application.TypeOfSupply = model.TypeOfSupply;

                    if (model.TypeOfSupply == "Unmetered Temporary Period (< 14 Days)")
                    {
                        application.ToDate = Convert.ToDateTime(model.ToDate);
                        application.FromDate = Convert.ToDateTime(model.FromDate);
                    }
                    if (model.TypeOfSupply != "Unmetered Temporary Period (< 14 Days)")
                    {
                        application.ToDate = DateTime.Now;
                        application.FromDate = DateTime.Now;
                    }

                    if (model.ApplicationType == "CHANGES")
                    {
                        application.ReasonForChange = model.reasonForChange;
                        application.ExistingMeterNo = model.ExistingMeterNo;
                        DateTime MyDateTime = DateTime.Now;
                        if (!string.IsNullOrEmpty(model.DateSupply))
                        {
                            //application.DateSupplyRequired = Convert.ToDateTime(model.DateSupply);
                            //Convert date to a current computer format.
                            CultureInfo MyCultureInfo = new CultureInfo("de-DE");
                            MyDateTime = (DateTime.Parse(model.DateSupply, MyCultureInfo));
                            application.DateSupplyRequired = MyDateTime;
                        }else
                        {
                            application.DateSupplyRequired = MyDateTime;
                        }
                        application.ExistingConnectionNo = model.ExistingConnectionNo;
                    }
                    application.RequestedDemand = model.RequestedDemand;
                    application.Preferred = model.prefferedConnectionType;
                    application.DateSupplyRequired = DateTime.Now;
                    application.Longitude = model.Longitude.ToString() == "" ? "N/A" : model.Longitude.ToString();
                    application.Latitude = model.Latitude.ToString() == "" ? "N/A" : model.Latitude.ToString();
                    application.ERFNo = model.ERFNo == null ? "N/A" : model.ERFNo;
                    application.PostalCode = model.PostalCode == null ? "N/A" : model.PostalCode;
                    application.StreetNumber = model.StreetNumber == null ? "N/A" : model.StreetNumber;
                    application.StreetName = model.StreetName == null ? "N/A" : model.StreetName;
                    application.UnitNumber = model.UnitNumber == null ? "N/A" : model.UnitNumber;
                    application.UnitName = model.UnitName == null ? "N/A" : model.UnitName;
                    application.Suburb = model.Suburb == null ? "N/A" : model.Suburb;
                    application.Town = model.Town == null ? "N/A" : model.Town;
                    application.ReasonForChange = model.reasonForChange == null ? "N/A" : model.reasonForChange;
                    application.ExistingMeterNo = model.ExistingMeterNo;
                    application.ExistingConnectionNo = model.ExistingConnectionNo == null ? "N/A" : model.ExistingConnectionNo;

                    application.IsActive = true;
                    application.IsDeleted = false;
                    application.Received = false;

                    db.tb_Application_History.Add(application);
                    db.SaveChanges();

                }
                catch (Exception ex)
                {
                    string error = "There was an error" + ex;
                }

            }


        }

        public void SaveMainSwitchDetails(ElectricalApplication model, int applicationId)
        {
            try
            {
                using (var db = new ElectricalConnectionDBEntities())
                {
                    var mainswitchdetails = new tb_MainSwitchDetails()
                    {
                        ApplicationId = applicationId,
                        ExistingCurrent = model.CurrentExistingApplication,
                        ExistingFaulRating = model.FaultRatingExistingApplication,
                        ExistingProtectionDevice = model.ProtectionDeviceExistingApplication,
                        ExistingVoltage = model.VoltageExistingApplication,
                        ProposedCurrent = model.CurrentProposedApplication,
                        ProposedFaulRating = model.FaultRatingProposedApplication,
                        ProposedProtectionDevice = model.ProtectionDeviceProposedApplication,
                        ProposedVoltage = model.VoltageProposedApplication
                    };
                    db.tb_MainSwitchDetails.Add(mainswitchdetails);
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                string error = "There was an error" + ex;
            }
        }

        public string AddDocuments(ElectricalApplication model, string FilePath, string FileName, string ContentType, int applicationId, string documentfor)
        {
            try
            {
                using (var db = new ElectricalConnectionDBEntities())
                {
                    tb_Documents document = new tb_Documents();

                    document.FileName = FileName;
                    document.FilePath = FilePath;
                    document.ApplicationId = applicationId;

                    document.ContentType = ContentType;
                    document.DocumentFor = documentfor;
                    document.DateCreated = DateTime.Now;
                    document.IsActive = true;
                    document.IsDeleted = false;

                    db.tb_Documents.Add(document);
                    db.SaveChanges();

                }
                return "Your Application Successful Sent!";
            }
            catch (Exception ex)
            {
                return "Error";
            }

        }

    }
}