﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Homer_MVC.Models;

namespace Homer_MVC.BusinessLogic
{
    public class ElectricityConnectionBL
    {
        private ElectricalConnectionDBEntities db = new ElectricalConnectionDBEntities();

        public int AddApplicant(ElectricalApplication model)
        {
          
                tb_Applicant app = new tb_Applicant();
                app.FullName = model.FullName;
                app.Cellphone = model.Cellphone;
                app.IDNo = model.IdNo;
                app.LandLine = model.LandLine;
                app.PostalAddress = model.PostalAddress;
                app.Suburb = model.Suburbs;
                app.PostalCode = model.PostalCodes;
                app.Email = model.Email;
                app.PreferedContact = model.prefferedConnectionType;

                app.IsActive = true;
                app.IsDeleted = false;

                db.tb_Applicant.Add(app);
                db.SaveChanges();
                return app.ApplicantId;

        }
        public int AddRepresentative(ElectricalApplication model)
        {
            tb_ElectricalRepresentative rep = new tb_ElectricalRepresentative();
            rep.Cellphone = model.Cellphone;
            rep.Email = model.Email;
            rep.PaostalCode = model.PostalCode;
            rep.Poastal_Address = model.PostalAddress;
            rep.RegistrationNumber = model.ElecRepresentativeRegistrationNo;
            rep.Landline = model.LandLine;

            rep.IsActive = true;
            rep.IsDeleted = false;

            db.tb_ElectricalRepresentative.Add(rep);
            db.SaveChanges();
            return rep.RepId;
        }
        public int AddSiteAffected(ElectricalApplication model)
        {
            tb_SiteAffected siteAffected = new tb_SiteAffected();
            siteAffected.ERFNo = model.ERFNo;
            siteAffected.PostalCode = model.PostalCode;
            siteAffected.StreetName = model.StreetName;
            siteAffected.StreetNumber = model.StreetNumber;
            siteAffected.Suburb = model.Suburb;
            siteAffected.Town = model.Town;
            siteAffected.UnitName = model.UnitName;
            siteAffected.UnitNumber = model.UnitNumber;
            siteAffected.Latitude = Convert.ToString(model.Latitude);
            siteAffected.Longitude = Convert.ToString(model.Longitude);

            siteAffected.IsActive = true;
            siteAffected.IsDeleted = false;

            db.tb_SiteAffected.Add(siteAffected);
            db.SaveChanges();
            return siteAffected.SiteAffectedId;
        }

        public int AddExistingApplication(ElectricalApplication model)
        {
            tb_DetailsOfMainSwitch obj = new tb_DetailsOfMainSwitch();

            obj.CurrentA = model.CurrentExistingApplication;
            obj.VoltageV = model.VoltageExistingApplication;
            obj.FaultRatingkA = model.FaultRatingExistingApplication;
            obj.ProtectionDevice = model.ProtectionDeviceExistingApplication;

            db.tb_DetailsOfMainSwitch.Add(obj);
            db.SaveChanges();
            
            return obj.MainSwitchID;
        }
        public int AddProposedApplication(ElectricalApplication model)
        {
            tb_DetailsOfMainSwitch obj = new tb_DetailsOfMainSwitch();

            obj.CurrentA = model.CurrentProposedApplication;
            obj.VoltageV = model.VoltageProposedApplication;
            obj.FaultRatingkA = model.FaultRatingProposedApplication;
            obj.ProtectionDevice = model.ProtectionDeviceProposedApplication;

            db.tb_DetailsOfMainSwitch.Add(obj);
            db.SaveChanges();

            return obj.MainSwitchID;
        }


        public string AddDetails(ElectricalApplication model,int? applicantId, int? representativeId, int? siteAffectedId, int? existingSiteId, int? proposedSiteId, int? existingApplicationId, int? proposedApplicationId)
        {
            try
            {
                tb_ConnectionDetails details = new tb_ConnectionDetails();

                details.ApplicantId = applicantId;
                details.RepId = representativeId;
                details.SiteAffectedId = siteAffectedId;             
                details.MeterTypes = model.MeterType;
                details.Tariff = model.Tariff;
                details.TarifScale = model.Tariff;

               
                details.RequestedPower = model.RequestedPower;
                details.NoOfPhases = model.NoOfPhases;
                details.TypeOfSupply = model.TypeOfSupply;

                if (model.TypeOfSupply == "Unmetered Temporary Period (< 14 Days)") 
                {
                    details.ToDate = Convert.ToDateTime(model.ToDate);
                    details.FromDate = Convert.ToDateTime(model.FromDate);
                }

                if (model.ApplicationType == "CHANGES")
                {
                    details.ReasonForChange = model.reasonForChange;
                    details.ExistingMeterNo = model.ExistingMeterNo;
                    if (!string.IsNullOrEmpty(model.DateSupply))
                    {
                        details.DateSupplyRequired = Convert.ToDateTime(model.DateSupply);
                    }
                    details.ExistingConnectionNo = model.ExistingConnectionNo;
                }
                details.RequestedDemand = model.RequestedDemand;
                details.Preferred = model.prefferedConnectionType;              
                details.ExistingEntireSite = null;
                details.ProposedEntireSite = null;
                details.ExistingForThisApplication = existingApplicationId;
                details.ProposedForThisApplication = proposedApplicationId;

                details.IsActive = true;
                details.IsDeleted = false;

                db.tb_ConnectionDetails.Add(details);
                db.SaveChanges();
                return "Your Application Successful Sent!";
            }
            catch
            {
                return "Error";
            }
        }

        public string SaveApplication(ElectricalApplication model)
        {
            try { 
            int applicantId = AddApplicant(model);
            int representativeId = AddRepresentative(model);
            int siteAffectedId = AddSiteAffected(model);
            int existingApplication = AddExistingApplication(model);
            int proposedApplication = AddProposedApplication(model);

            string resultmessage = AddDetails(model, applicantId, representativeId, siteAffectedId, null, null, existingApplication, proposedApplication);
            return resultmessage;
            }

            catch (Exception ex)
            {
                return "There is an error";
            }
        }
    }
}
